# Heroku database backup

A script that backs up a Heroku database using restic.


## Runnning this

To run this, you need to provide a few environment variables:

* `RESTIC_REPOSITORY`: The URL of the restic repository (e.g.  `sftp:ab1234@ab1234.rsync.net:backups/test`)
* `RESTIC_PASSWORD`: The password to the restic repository.
* `DATABASE_URL`: The URL of the PostgreSQL database to connect to.
* `RESTIC_MONITORING_URL`: The URL to ping after backup is done (e.g. something on healthchecks.io).
* `BACKUP_SSH_PRIVATE_KEY`: The private SSH key to the backup server, encoded as base64 (`openssl enc -base64 -in my.key -out my.key.base64`).
