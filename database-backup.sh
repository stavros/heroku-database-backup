#!/usr/bin/env bash

set -euo pipefail

RESTIC_VERSION="0.12.1"

# Extract the protocol.
proto=$(echo "$DATABASE_URL" | grep '://' | sed -e's,^\(.*://\).*,\1,g')
# Remove the protocol.
url=$(echo "$DATABASE_URL" | sed -e s,"$proto",,g)

# Extract the user and password (if any).
userpass=$(echo "$url" | grep @ | cut -d@ -f1)
pass=$(echo "$userpass" | grep : | cut -d: -f2)
if [ -n "$pass" ]; then
    user=$(echo "$userpass" | grep : | cut -d: -f1)
else
    user=$userpass
fi

# Extract the host.
hostport=$(echo "$url" | sed -e s,"$userpass"@,,g | cut -d/ -f1)
port=$(echo "$hostport" | grep : | cut -d: -f2)
if [ -n "$port" ]; then
    host=$(echo "$hostport" | grep : | cut -d: -f1)
else
    host=$hostport
fi

# Extract the databse (if any).
database=$(echo "$url" | grep / | cut -d/ -f2-)


TEMP_DIR=$(mktemp -d -t dbbackup-XXXXXXXXXX)

cd "$TEMP_DIR" || exit

mkdir ~/.ssh || true
ssh-keyscan "$(echo "$RESTIC_REPOSITORY" | sed 's/.*@\(.*\)\:.*/\1/g')" > ~/.ssh/known_hosts
echo "$BACKUP_SSH_PRIVATE_KEY" | openssl enc -a -d -out ~/.ssh/id_ed25519

if [ -x "$(which restic)" ] ; then
    echo "restic found."
    RESTIC_BIN="$(which restic)"
else
    echo "restic not found, downloading..."
    wget https://github.com/restic/restic/releases/download/v${RESTIC_VERSION}/restic_${RESTIC_VERSION}_linux_amd64.bz2 -O restic.bz2
    bunzip2 restic.bz2
    chmod +x restic
    RESTIC_BIN="$TEMP_DIR/restic"
fi

mkdir backup
cd backup
pg_dump -f output.sql "host=$host port=$port dbname=$database user=$user password=$pass"

echo "Starting backup to $RESTIC_REPOSITORY..."
$RESTIC_BIN init || true

$RESTIC_BIN backup .

rm output.sql

echo "Pruning..."

$RESTIC_BIN forget --prune \
    --keep-last=3 \
    --keep-daily=7 \
    --keep-weekly=4 \
    --keep-monthly=12 \
    --keep-yearly=1 \
    --keep-within=3d

$RESTIC_BIN check

# Inform the snitch.
curl -fsS --retry 3 "$RESTIC_MONITORING_URL" > /dev/null 2>&1
